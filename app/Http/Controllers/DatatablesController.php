<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use yajra\Datatables\Datatables;
use App\Url;

class DatatablesController extends Controller {
	public function getIndex() {
		return view('list');
	}

	public function anyData() {
		return Datatables::of(Url::select('*'))->make(true);
	}

}
