<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Url;
use Validator;
use Redirect;
use yajra\Datatables\Html\Builder;

class ShortenController extends Controller {

    // protected $htmlBuilder;

    // public function __construct(Builder $htmlBuilder) {
    //     $this->htmlBuilder = $htmlBuilder;
    // }

    // public function list() {
    //     return view('list');
    // }

    // public function listData() {
    //     $urls = Url::select(['url','visits','hash','created_at']);

    //     return Datatables::of($urls)->make();
    // }

    public function getIndex() {
        return view('list');
    }

    public function anyData() {
        return Datatables::of(Url::select('*'))->make(true);
    }

    public function process($data) {
        $validator = Validator::make($data, [
            'url' => 'unique:urls,url',
            ]);
        $url = $data['url'];
        if ($validator->fails()) {
            $urlObject = Url::where('url', $url)->first();
        } else {
            $urlObject = new Url;
            $urlObject->url = $url;
            $urlObject->save();

            $hash = base_convert($urlObject->id, 10, 36);
            $urlObject->hash = $hash;
            $urlObject->save();
        }

        return Redirect::to('/')->with('urlObject', $urlObject);
    }

}
