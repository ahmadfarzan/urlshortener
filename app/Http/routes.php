<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	return view('main');
});
Route::controller('list_all', 'DatatablesController', [
    'anyData'  => 'datatables.data',
    'getIndex' => 'datatables',
]);

Route::get('shorten_url', function () {
	return redirect('/');
});
Route::post('shorten_url', function () {
	$rules = array(
		'url' => 'required|url',
		);
	$data = Input::all();
	$validator = Validator::make($data, $rules);
	if ($validator->fails()) {
		$messages = $validator->messages();
		return Redirect::to('/')->withInput()->withErrors($messages->getMessages());
	} else {
		return (new App\Http\Controllers\ShortenController())->process($data);
	}
});

Route::any('{catchall}+', function ( $hash ) {
	$urlObject = \App\Url::where('hash', $hash)->first();
	if($urlObject) {
		return Redirect::to('/')->with('urlObject', $urlObject);
	} else {
		return redirect('/');
	}
} )->where('catchall', '[0-9a-z]*');

Route::any('{catchall}/', function ( $hash ) {
	$urlObject = \App\Url::where('hash', $hash)->first();
	if($urlObject) {
		$urlObject->visits++;
		$urlObject->save();
		return redirect($urlObject->url);
	} else {
		return redirect('/');
	}
} )->where('catchall', '[0-9a-z]*');