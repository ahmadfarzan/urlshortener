<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Url;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('URLsSeeder');
        $this->command->info('URLs table seeded!');

        Model::reguard();
    }
}

class URLsSeeder extends Seeder {
    public function run() {
        $faker = Faker\Factory::create();
        for ($i=1; $i <= 10000; $i++) {
            $faker->seed();
            $url = $faker->url;

            $urlObject = Url::where('url', $url)->first();

            if(!$urlObject) {
                $urlObject = new Url;
                $urlObject->url = $url;
                $urlObject->save();

                $hash = base_convert($urlObject->id, 10, 36);
                $urlObject->hash = $hash;
                $urlObject->save();
            }
        }
    }
}
