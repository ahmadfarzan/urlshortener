<!DOCTYPE html>
<html>
<head>
	<title>URL Shortener</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="URL Shortener for MindValley">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	<style type="text/css">
	body {
		background-color: #eee;
	}
	.urlDetailsContainer {
		margin-top: 30px;
	}
	.urlDetailsContainer p {
		font-size: 13px;
	}
	.urlDetailsContainer p span {
		font-weight: bold;
	}
	.urlDetailsContainer input[type='text'] {
		cursor: text;
	}
	</style>
</head>
<body>
	<div class="container">
		@yield('content')
	</div>
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	@stack('scripts')
</body>
</html>