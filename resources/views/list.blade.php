@extends('template')

@section('content')
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 text-center">
        <a class="" href="{!! url('/') !!}">[Homepage]</a>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        <h3 class="text-center">List of all Shortened URLs</h3>
    </div>
</div>
<table class="table table-bordered" id="urls-table">
    <thead>
        <tr>
            <th>Tini URL</th>
            <th>Status URL</th>
            <th class="desktop">Original URL</th>
            <th class="mobile-l">Visits</th>
            <th>Generated</th>
        </tr>
    </thead>
</table>
@stop

@push('scripts')
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script>
    $(function() {
        $('#urls-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('datatables.data') !!}',
            columns: [
            { data: 'hash', name: 'hash' },
            { data: 'hash', name: 'hash' },
            { data: 'url', name: 'url' },
            { data: 'visits', name: 'visits' },
            { data: 'created_at', name: 'created_at' }
            ],
            "order": [[ 4, "desc" ]],
            "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return "{!!url('/')!!}/"+data;
                },
                "width": "20%",
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    return "{!!url('/')!!}/"+data+"+";
                },
                "width": "20%",
                "targets": 1
            },
            ]
        });
    });
</script>
@endpush