@extends('template')

@section('content')
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 text-right">
        <a class="" href="{!! url('list_all') !!}">[List of All URLs]</a>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        <h3 class="text-center">URLs to Shorten</h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        {!! Form::open(array('url' => 'shorten_url', 'class' => 'form', 'id' => 'urlShortenForm')) !!}
        @if(!$errors->isEmpty())
        <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
            <p>{{{$error}}}</p>
            @endforeach
        </div>
        @endif
        <div class="form-group">
            {!! Form::text('url', null, array('required', 'class'=>'form-control', 'placeholder'=>'URL to Shorten')) !!}
        </div>
        <button type="submit" class="btn btn-primary pull-right">Submit</button>
        {!! Form::close() !!}
    </div>
</div>
<div class="row urlDetailsContainer">
    <div class="col-sm-4 col-sm-offset-4">
        @if(Session::has('urlObject'))
        <p class="text-center"><img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(200)->backgroundColor(238,238,238)->margin(1)->generate(url(Session::get('urlObject')->hash))) }} "></p>

        <div class="form-group">
            {!! Form::label('url', 'Tini URL') !!}
            {!! Form::text('url', url(Session::get('urlObject')->hash), array('readonly', 'class'=>'form-control')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('statusurl', 'Status URL') !!}
            {!! Form::text('statusurl', url(Session::get('urlObject')->hash.'+'), array('readonly', 'class'=>'form-control')) !!}
        </div>

        <p><span>Original URL: </span>{!! HTML::link(url(Session::get('urlObject')->url), url(Session::get('urlObject')->url), array('target' => '_blank')) !!}</p>
        <p><span>Generated: </span>{{{date("d F Y h:i A",strtotime(Session::get('urlObject')->created_at))}}}</p>
        <p><span>Visits: </span>{{{(int)Session::get('urlObject')->visits}}}</p>
        @endif 
    </div>
</div>
@stop

@push('scripts')
<script type="text/javascript">
$(".urlDetailsContainer input[type='text']").on("click", function () {
        $(this).select();
    });
</script>
@endpush